PIC18F97J94 MA180034 PIM and DM240001-3
====================================

This is a minimum demo project to show the MA180034 PIM working with the DM240001-3 16/32 Explorer board.

 Description:

- Support for the 16x2 LCD character module on the DM240001-3.
- Support for UART2 connected to the serial interface on the DM240001-3.
- Support for LED_D2 & LED_D3 on the MA180034 PIM.
- Support for S2  on the MA180034 PIM.

Notes:

- External device programmer is required as the on board ICD of the DM240001-3 does not work with the MA180034 PIM.
- The analog output voltage for the potentiometer and temperature sensor are connected to the same pins as the ICD interface on the MA180034 PIM.