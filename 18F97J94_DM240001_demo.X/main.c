/*
 * File:   main.c
 * Author: CHarles Ader
 * Target: PIC18F97J94
 * Compiler: XC8 v2.46
 * IDE: MPLABX v6.20
 *
 * Created on June 22, 2024, 10:06 PM
 * 
 *                                                               PIC18F97J94
 *            +---------------+               +---------------+               +---------------+             +---------------+
 *   LCDD4  1 : RH2           :            26 : RH5           :            51 : RJ6           :          76 : RJ1           :
 *          2 : RH3           :            27 : RH4           :            52 : RJ7           :     PWMB 77 : RJ0           :
 *   LCDD5  3 : RE1 RP29      :        3v3 28 : VUSB3v3       :        RXA 53 : RC2 RP11      :     EECS 78 : RD7 RP27      :
 *   LCDD6  4 : RE0 RP28      :    *VBAT_1 29 : VBAT          :        SCL 54 : RC3 RP15      :     LED5 79 : RD6 RP26      :
 *     3v3  5 : VDD           :            30 : RL5           :       LED7 55 : RK1           :       S4 80 : RK6           :
 *     CSA  6 : RG0 RP46      :        3v3 31 : VDD           :        SDA 56 : RC4 RP17      :     LED6 81 : RD5 RP25      :
 *          7 : RG1 RP39      :        GND 32 : VSS           :      MISOB 57 : RC5 RP16      :     LCDE 82 : RD4 RP24      :
 *     CC1  8 : RG2 RP42      :        CSB 33 : RA3 RP3       : (TEMP)*PGD 58 : RB7/PGD       :    LCDRW 83 : RD3 RP23      :
 *     CC2  9 : RG3 RP43      :            34 : RA2 RP2       :        3v3 59 : VDD           :       S3 84 : RD2 RP22      :
 *         10 : RL1           :        GND 35 : VSS           :       LED8 60 : RK2           :       S6 85 : RK7           :
 * *S1*VPP 11 : MCLR          :      MISOA 36 : RA1 RP1       :   XTL_8MHz 61 : RA7/OSC1 RP10 :     LED9 86 : RD1 RP21      :
 *   MOSIA 12 : RG4 RP44      :            37 : RA0 RP0       :   XTL_8MHz 62 : RA6/OSC2 RP6  :      GND 87 : VSS           :
 *         13 : RL2           :       LED4 38 : RL6           :            63 : RK3           :      3v3 88 : VDD           :
 *     GND 14 : VSS           :        GND 39 : VSS           :        GND 64 : VSS           :          89 : RG6           :
 *    10uf 15 : VCAP          :        3v3 40 : VDD           :  (POC)*PGC 65 : RB6/PGC       : S5/LED10 90 : RD0 RP20      :
 *         16 : RL3           :            41 : RL7           :            66 : RK4           :     RSTA 91 : RL0           :
 *    SCKA 17 : RF7 RP38      :      MOSIB 42 : RA5 RP5       :            67 : RB5 RP13      :    LCDD7 92 : RE7 RP31      :
 *    INTB 18 : RF6 RP40      :            43 : RA4 RP4       :       INTA 68 : RB4 RP12      :    LCDD0 93 : RE6 RP34      :
 *    RSTB 19 : RF5 RP35      : *XTL_32KHz 44 : RC1/SOSCI     :       SCKB 69 : RB3 RP7       :    LCDD1 94 : RE5 RP37      :
 *     *D+ 20 : RF4/D+        : *XTL_32KHz 45 : RC0/SOSCO     :        TXA 70 : RB2 RP14      :    LCDD2 95 : RE4 RP32      :
 *    LED3 21 : RL4           :            46 : RK0           :            71 : RK5           :      *S2 96 : RG7           :
 *     *D- 22 : RF3/D?        :        RXB 47 : RC6 RP18      :    *LED_D3 72 : RB1 RP9       :    LCDD3 97 : RE3 RP33      :
 *         23 : RF2 RP36      :        TXB 48 : RC7 RP19      :    *LED_D2 73 : RB0/INT0 RP8  :          98 : RE2 RP30      :
 *     ANB 24 : RH7           :       PWMA 49 : RJ4           :            74 : RJ3           :          99 : RH0           :
 *     ANA 25 : RH6           :            50 : RJ5           :      LCDRS 75 : RJ6           :         100 : RH1           :
 *            +---------------+               +---------------+               +---------------+             +---------------+
 *                                                               TQFP-100
 * 
 *  Note: Resources marked with (*) are located on the MA180034 PIM circuit board.
 * 
 *  Most resources are located on the DM240001-3 16/32 Explorer Board.
 *  The analog (TEMP) and (POT) resources on the DM240001-3 
 *  are not available as the pins are used by the device programming 
 *  interface on the MA180034 PIM.
 * 
 *  Also the device programmer on the DM240001-3 does not work
 *  with the MA180034 PIM. A standalone ICD is required.
 */

/* Define system oscillator frequency this code will setup */
#define _XTAL_FREQ (64000000ul)
#define FCYC (_XTAL_FREQ/4ul)

/* Select UART1 baud rate */
#define BAUD_RATE (9600UL)

// CONFIG1L
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset (Enabled)
#pragma config XINST = OFF      // Extended Instruction Set (Disabled)

// CONFIG1H
#pragma config BOREN = OFF      // Brown-Out Reset Enable (Disabled in hardware)
#pragma config BORV = 1         // Brown-out Reset Voltage (1.8V)
#pragma config CP0 = OFF        // Code Protect (Program memory is not code-protected)

// CONFIG2L
#pragma config FOSC = FRCPLL    // Oscillator (Fast RC Oscillator with PLL module (FRCPLL))
#pragma config SOSCSEL = LOW    // T1OSC/SOSC Power Selection Bits (Low Power T1OSC/SOSC circuit selected)
#pragma config CLKOEN = OFF     // Clock Out Enable Bit (CLKO output disabled on the RA6 pin)
#pragma config IESO = ON        // Internal External Oscillator Switch Over Mode (Enabled)

// CONFIG2H
#pragma config PLLDIV = DIV2    // PLL Frequency Multiplier Select bits (96 MHz PLL selected; Oscillator divided by 2 (8 MHz input))

// CONFIG3L
#pragma config POSCMD = NONE    // Primary Oscillator Select (Primary oscillator disabled)
#pragma config FSCM = CSECMD    // Clock Switching and Monitor Selection Configuration bits (Clock switching is enabled, fail safe clock monitor is disabled)

// CONFIG3H

// CONFIG4L
#pragma config WPFP = WPFP255   // Write/Erase Protect Page Start/End Boundary (Write Protect Program Flash Page 255)

// CONFIG4H
#pragma config WPDIS = WPDIS    // Segment Write Protection Disable (Disabled)
#pragma config WPEND = WPENDMEM // Segment Write Protection End Page Select (Write Protect from WPFP to the last page of memory)
#pragma config WPCFG = WPCFGDIS // Write Protect Configuration Page Select (Disabled)

// CONFIG5L
#pragma config T5GSEL = T5G     // TMR5 Gate Select bit (TMR5 Gate is driven by the T5G input)
#pragma config CINASEL = DEFAULT// CxINA Gate Select bit (C1INA and C3INA are on their default pin locations)
#pragma config EASHFT = ON      // External Address Shift bit (Address Shifting enabled)
#pragma config ABW = MM         // Address Bus Width Select bits (8-bit address bus)
#pragma config BW = 16          // Data Bus Width (16-bit external bus mode)
#pragma config WAIT = OFF       // External Bus Wait (Disabled)

// CONFIG5H
#pragma config IOL1WAY = OFF    // IOLOCK One-Way Set Enable bit (the IOLOCK bit can be set and cleared using the unlock sequence)
#pragma config LS48MHZ = SYSX2  // USB Low Speed Clock Select bit (Divide-by-2 (System clock must be 12 MHz))
#pragma config MSSPMSK2 = MSK7  // MSSP2 7-Bit Address Masking Mode Enable bit (7 Bit address masking mode)
#pragma config MSSPMSK1 = MSK7  // MSSP1 7-Bit Address Masking Mode Enable bit (7 Bit address masking mode)

// CONFIG6L
#pragma config WDTWIN = PS25_0  // Watch Dog Timer Window (Watch Dog Timer Window Width is 25 percent)
#pragma config WDTCLK = FRC     // Watch Dog Timer Clock Source (Use FRC when WINDIS = 0, system clock is not INTOSC/LPRC and device is not in Sleep; otherwise, use INTOSC/LPRC)
#pragma config WDTPS = 32768    // Watchdog Timer Postscale (1:32768)

// CONFIG6H
#pragma config WDTEN = OFF      // Watchdog Timer Enable (WDT disabled in hardware; SWDTEN bit disabled)
#pragma config WINDIS = WDTSTD  // Windowed Watchdog Timer Disable (Standard WDT selected; windowed WDT disabled)
#pragma config WPSA = 128       // WDT Prescaler (WDT prescaler ratio of 1:128)

// CONFIG7L
#pragma config RETEN = OFF      // Retention Voltage Regulator Control Enable (Retention not available)
#pragma config VBTBOR = OFF     // VBAT BOR Enable (VBAT BOR is disabled)
#pragma config DSBOREN = OFF    // Deep Sleep BOR Enable (BOR disabled in Deep Sleep (does not affect operation in non-Deep Sleep modes))
#pragma config DSBITEN = OFF    // DSEN Bit Enable bit (Deep Sleep operation is always disabled)

// CONFIG7H

// CONFIG8L
#pragma config DSWDTPS = DSWDTPS1F// Deep Sleep Watchdog Timer Postscale Select (1:68719476736 (25.7 Days))

// CONFIG8H
#pragma config DSWDTEN = OFF    // Deep Sleep Watchdog Timer Enable (DSWDT Disabled)
#pragma config DSWDTOSC = LPRC  // DSWDT Reference Clock Select (DSWDT uses LPRC as reference clock)

#include <xc.h>
/*
 * ISR handlers
 */
void __interrupt(low_priority) Low_ISR(void)
{
    
}
void __interrupt(high_priority) High_ISR(void)
{
    
}
/*
 * Initialize this PIC
 */
void PIC_Init(void)
{
    INTCON = 0;
    OSCCON  = 1;
    OSCCON2 = 2;
    OSCCON3 = 0;
    OSCCON4 = 0x20;
    RCONbits.IPEN = 1;

    ANCON1 = 0;
    ANCON2 = 0;
    ANCON3 = 0;
    
    TRISBbits.TRISB0 = 0;
    TRISBbits.TRISB1 = 0;
    LATBbits.LATB0 = 0;
    LATBbits.LATB1 = 0;
    
}
/* Define GPIO pins connected to the LCD character module. */
#define LCD_EN LATDbits.LATD4
#define LCD_RW LATDbits.LATD3
#define LCD_RS LATJbits.LATJ6
#define LCD_D7 LATEbits.LATE7
#define LCD_D6 LATEbits.LATE0
#define LCD_D5 LATEbits.LATE1
#define LCD_D4 LATHbits.LATH2
#define LCD_EN_DIR(x) do {TRISDbits.TRISD4=x;}while(0)
#define LCD_RW_DIR(x) do {TRISDbits.TRISD3=x;}while(0)
#define LCD_RS_DIR(x) do {TRISJbits.TRISJ6=x;}while(0)
#define LCD_D7_DIR(x) do {TRISEbits.TRISE7=x;}while(0)
#define LCD_D6_DIR(x) do {TRISEbits.TRISE0=x;}while(0)
#define LCD_D5_DIR(x) do {TRISEbits.TRISE1=x;}while(0)
#define LCD_D4_DIR(x) do {TRISHbits.TRISH2=x;}while(0)
 
/* Start address of each line of 20x4 LCD module */
#define LINE_ONE    0x00u
#define LINE_TWO    0x40u
/*
 * LCR enable pulse
 */
void LCD_EN_Pulse(void)
{
    LCD_EN=1;
    __delay_us(2);
    LCD_EN=0;
    __delay_us(43);
}
/*
 * LCD put data as two 4-bit nibbles
 */
void LCD_Put(uint8_t Data)
{
    LCD_EN=0;LCD_D7=0;LCD_D6=0;LCD_D5=0;LCD_D4=0;
    if(Data & 0x80)LCD_D7=1;
    if(Data & 0x40)LCD_D6=1;
    if(Data & 0x20)LCD_D5=1;
    if(Data & 0x10)LCD_D4=1;
    LCD_EN_Pulse();
    LCD_D7=0;LCD_D6=0;LCD_D5=0;LCD_D4=0;
    if(Data & 0x8)LCD_D7=1;
    if(Data & 0x4)LCD_D6=1;
    if(Data & 0x2)LCD_D5=1;
    if(Data & 0x1)LCD_D4=1;
    LCD_EN_Pulse();
    if(!LCD_RS) __delay_ms(4);
}
/*
 * LCD Write character to the module
 */
void LCD_Write(uint8_t Data)
{
    LCD_RS = 1;
    LCD_Put(Data);
}
/*
 * LCD Write a zero terminated string of characters
 */
void LCD_WriteString(char * pStr)
{
    if((pStr) && (*pStr)) do {
        LCD_Write(*pStr++);
    } while (*pStr);
}
/*
 * LCD set the line and character position for output data
 */
void LCD_SetPosition(unsigned char data)
{
    LCD_RS = 0;
    LCD_Put(data | 0x80u);
}
/*
 * Initialize LCD character module (HD44780 in 4-bit parallel mode)
 */
void LCD_Init(void)
{
    LCD_RW_DIR(0);LCD_RS_DIR(0);LCD_EN_DIR(0);LCD_D7_DIR(0);LCD_D6_DIR(0);LCD_D5_DIR(0);LCD_D4_DIR(0);
    LCD_RW=0;LCD_RS=0;LCD_EN=0;LCD_D7=0;LCD_D6=0;LCD_D5=1;LCD_D4=1;
    __delay_ms(100); /*100ms,HD44780 LCD Power on delay*/
    LCD_EN_Pulse();
    LCD_EN_Pulse();
    LCD_EN_Pulse();
    LCD_D4=0;
    LCD_EN_Pulse();
    
    LCD_Put(0x28); /*use 2 line and initialize 5*8 matrix in (4-bit mode)*/
    LCD_Put(0x01); /*clear display screen*/
    LCD_Put(0x0c); /*display on cursor off*/
    LCD_Put(0x06); /*increment cursor (shift cursor to right)*/
}
/*
 * UART2
 * 
 * Use RC7/RP19 as TXD, RC6/RP18 as RXD
 */
void U2_Init(void)
{
    PIE1bits.TX1IE = 0;     /* disable transmitter interrupts */
    PIE1bits.RC1IE = 0;     /* disable receiver interrupts */
    BAUDCON2 = 0;            /* stop UART1 */
    RCSTA2   = 0;
    TXSTA2   = 0;
    TRISCbits.TRISC6 = 1;
    TRISCbits.TRISC7 = 0;
    LATCbits.LATC7   = 1;
    RPINR2_3  = (unsigned char)( 4 | ( 4<<4));   /* U2_RXD input assigned to RP18, U2_TXD input assigned to RP19  */
    RPOR18_19 = (unsigned char)( 0 | ( 2<<4));   /* RP19 assigned to U2_TXD output */
    
    
    TXSTA2bits.TX9  = 0;     /* use 8-bit data transmission */
    TXSTA2bits.SYNC = 0;     /* use asynchronous mode */
    TXSTA2bits.BRGH = 1;     /* use high speed BRG mode, see errata */
    RCSTA2bits.RX9  = 0;     /* use 8-bit data reception */
    BAUDCON2bits.BRG16 = 1;  /* use 16-bit baud rate divisor */
    
    /* set the baud rate */
    SPBRGH2 = (unsigned char)(((FCYC/BAUD_RATE)-1U) >> 8U);
    SPBRG2  = (unsigned char)((FCYC/BAUD_RATE)-1U);
    
    TXSTA2bits.TXEN = 1;     /* enable transmitter */
    RCSTA2bits.CREN = 1;     /* enable receiver */
    RCSTA2bits.SPEN = 1;     /* enable UART1 */
}
/*
 * Send byte to UART2
 */
void U2_Write(uint8_t txData)
{
    while(!TXSTA2bits.TRMT)
    {
        CLRWDT();
    }
 
    TXREG2 = txData;    // Write the data byte to the USART.
}
/*
 * UART2 Write a zero terminated string of characters
 */
void U2_WriteString(char * Str)
{
    if(Str) if(*Str) do
    {
        U2_Write(*Str++);
    } while (*Str);
}
/*
 * main application
 */
void main(void) 
{
    /*
     * Application initialization
     */
    PIC_Init();
    LCD_Init();
    U2_Init();
    
    LCD_SetPosition(LINE_ONE);
    LCD_WriteString("PIC18F97J94 LCD ");
    LCD_SetPosition(LINE_TWO);
    LCD_WriteString(" built " __TIME__);

   __delay_ms(10);
    
    U2_WriteString("\r\nPIC18F97J94 UART/LCD test built on " __DATE__ " at " __TIME__ "\r\n");
 
    /*
     * Application process loop
     */
    for(;;)
    {
        uint8_t ToggleWait;
        for(ToggleWait = 25; ToggleWait; ToggleWait--)
        {
            LCD_SetPosition(LINE_TWO);
            __delay_ms(20);
            /* Show the state of the S2 switch on the PIM */
            if(PORTGbits.RG7) 
            {
                LATBbits.LATB1 = 0; /* turn off LED_D3 */
                LCD_Write('0');
            }
            else 
            {
                LATBbits.LATB1 = 1; /* turn on LED_D3 */
                LCD_Write('1');
            }
        }
        LATBbits.LATB0 ^= 1;    /* toggle LED_D2 to say we are alive */
    }
}
